library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library vunit_lib;
context vunit_lib.vunit_context;
library osvvm;
use osvvm.CoveragePkg.all;

library uut_lib;

entity tb_uut is
	generic (
		RUNNER_CFG : string   := RUNNER_CFG_DEFAULT;
		DATA_WIDTH : positive := 8;
		SIM_CYCLES : positive := 5
	);
end entity tb_uut;

architecture RTL of tb_uut is

	signal clk  : std_logic := '0';
	signal ce   : std_logic;
	signal data : std_logic_vector(DATA_WIDTH-1 downto 0);
	
	shared variable cov_bin : CovPType;
	
begin

	clk <= not clk after 10 ns;
	
	sim_proc : process
		variable last_data : unsigned(data'range) := (others => '0');
	begin
		
		test_runner_setup(runner, RUNNER_CFG);
		cov_bin.AddBins(GenBin(0, 2**DATA_WIDTH-1));

		ce <= '1';
		wait until rising_edge(clk);

		for i in 1 to SIM_CYCLES loop
			wait until rising_edge(clk);
			assert data = std_logic_vector(last_data + 1)
				report "Wrong data!"
				severity error;
			cov_bin.ICover(to_integer(unsigned(data)));
			last_data := unsigned(data);
		end loop;
		
		-- Enable to show coverage report
		--cov_bin.WriteBin;
		info("Functional Coverage: " & real'image(cov_bin.GetCov) & "%");
		test_runner_cleanup(runner);
	end process;
	
	uut : entity uut_lib.uut
		generic map(
			DATA_WIDTH => data'length
		)
		port map(
			clk_in => clk,
			ce_in  => ce,
			data_q => data
		);

end architecture RTL;
